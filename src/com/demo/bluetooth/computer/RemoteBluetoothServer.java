package com.demo.bluetooth.computer;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.TransferHandler;

public class RemoteBluetoothServer{
	private static final int CURSOR_UP = 10;
	private static final int CURSOR_RIGHT = 20;
	private static final int CURSOR_DOWN = 30;
	private static final int CURSOR_LEFT = 40;

	public static ArrayList<WaitThread> connections;
	public static ArrayList<ProcessConnectionThread> connections2;
	public static ArrayList<JFrame> frames;
	public static CommandLineThread mCommandLineThread;
	public static BufferedReader br;
	
	public static void main(String[] args) {
		br = new BufferedReader(new InputStreamReader(System.in));
		connections = new ArrayList<WaitThread>();
		mCommandLineThread = new CommandLineThread();
		frames = new ArrayList<JFrame>();
		
	    class DragMouseAdapter extends MouseAdapter {
	        public void mousePressed(MouseEvent e) {
	            JComponent c = (JComponent) e.getSource();
	            TransferHandler handler = c.getTransferHandler();
	            handler.exportAsDrag(c, e, TransferHandler.COPY);
	        }
	    }
	    class MoveMeMouseHandler extends MouseAdapter {
	        private int xOffset;
	        private int yOffset;
	        private JLabel draggy;
	        private String oldText;

	        @Override
	        public void mouseReleased(MouseEvent me) {
	            if (draggy != null) {
	                draggy.setText(oldText);
	                draggy.setSize(draggy.getPreferredSize());
	                draggy = null;
	            }
	        }

	        public void mousePressed(MouseEvent me) {
	            JComponent comp = (JComponent) me.getComponent();
	            Component child = comp.findComponentAt(me.getPoint());
	            if (child instanceof JLabel) {
	                xOffset = me.getX() - child.getX();
	                yOffset = me.getY() - child.getY();

	                draggy = (JLabel) child;
	                oldText = draggy.getText();
	                draggy.setSize(draggy.getPreferredSize());
	            }
	        }

	        public void mouseDragged(MouseEvent me) {
	            if (draggy != null) {
	                draggy.setLocation(me.getX() - xOffset, me.getY() - yOffset);
	            }
	        }
	    }
	    class MoveMePane extends JLayeredPane {
			private static final long serialVersionUID = 1L;

			public MoveMePane() {
	            int width = 400;
	            int height = 400;
	            for (int index = 0; index < 1; index++) {
	                JLabel label = new JLabel(new ImageIcon("ic_add.png"));
	                label.setSize(label.getPreferredSize());

	                int x = (int) Math.round(Math.random() * width);
	                int y = (int) Math.round(Math.random() * height);
	                if (x + label.getWidth() > width) {
	                    x = width - label.getWidth();
	                }
	                if (y + label.getHeight() > width) {
	                    y = width - label.getHeight();
	                }
	                label.setLocation(x, y);
//	                connections2.get(0).write(x, y);
	                add(label);
	            }

	            MoveMeMouseHandler handler = new MoveMeMouseHandler();
	            addMouseListener(handler);
	            addMouseMotionListener(handler);
	        }

	        @Override
	        public Dimension getPreferredSize() {
	            return new Dimension(400, 400);
	        }
	    }
		System.out.println("BluetoothServer Launched.");
		while(true) {
			try {
				String line  = br.readLine();
				if(line.equals("connect")) {
					System.out.println("Instantiating Runnable");
					System.out.println("Ran Runnable(), instantiating WaitThread");
					WaitThread wt = new WaitThread();
					connections.add(wt);
					connections.get(connections.size()-1).start();
					JFrame frame = new JFrame();
	                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	                frame.setLayout(new BorderLayout());
	                frame.add(new MoveMePane());
	                frame.pack();
	                frame.setLocationRelativeTo(null);
	                frame.setVisible(true);
//					JFrame window = new JFrame();
//					window.setSize(100, 250);
//					window.setTitle(String.valueOf(connections.size()-1));
//					JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 50, 15));
//					ImageIcon img1 = new ImageIcon("ic_add.png");
//					JLabel label1 = new JLabel(img1, JLabel.CENTER);
//					MouseListener list = new DragMouseAdapter();
//					label1.addMouseListener(list);
//					panel.addMouseListener(list);
//					panel.setTransferHandler(new TransferHandler("icon"));
//					label1.setTransferHandler(new TransferHandler("icon"));
//					panel.add(label1);
//					window.add(panel);
//					window.setVisible(true);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(mCommandLineThread.hasBeenStarted == false) {
				mCommandLineThread.run();
			}
		}
	}
}
