package com.demo.bluetooth.computer;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import javax.microedition.io.StreamConnection;

//import org.omg.CORBA.DataOutputStream;

public class ProcessConnectionThread extends Thread {

	private StreamConnection mConnection;

	// Constant that indicate command from devices
	private static final int EXIT_CMD = -1;
	private static final int CURSOR_UP = 10;
	private static final int CURSOR_RIGHT = 20;
	private static final int CURSOR_DOWN = 30;
	private static final int CURSOR_LEFT = 40;

	private InputStream dis;
	private OutputStream dos;

	public ProcessConnectionThread(StreamConnection connection) {
		mConnection = connection;
	}

	@Override
	public void run() {
		try {
			// prepare to receive data
			//			InputStream inputStream = mConnection.openInputStream();
			dis = mConnection.openDataInputStream();
			dos = mConnection.openDataOutputStream();

			System.out.println("waiting for input");

			while (true) {
				byte[] b = new byte[1024];
				dis.read(b);
				String temp = new String(b);
				System.out.println(temp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process the command from client
	 * @param command the command code
	 */
	private void processCommand(int command) {
		try {
			Robot robot = new Robot();
			switch (command) {
			case CURSOR_UP:
				break;
				/*case KEY_RIGHT:
				robot.keyPress(KeyEvent.VK_RIGHT);
				System.out.println("Right");
				robot.keyRelease(KeyEvent.VK_RIGHT);
				break;
			case KEY_LEFT:
				robot.keyPress(KeyEvent.VK_LEFT);
				System.out.println("Left");
				robot.keyRelease(KeyEvent.VK_LEFT);
				break;*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void write(String message) {
		System.out.println("Writing "+message);
		try {
			dos.write(message.getBytes());
			//			dos.close();
		} catch (Exception e) {
			System.out.println("Failed to write "+message);
			e.printStackTrace();
		}
	}

	public void write(int command) {
		try {
			System.out.println("Attempting to write command to output");
			dos.write(command);
		} catch (Exception e) {
			System.out.println("Failed to write command");
		}
	}
	
	public void write(SerializablePoint sp) {
//		dos.write(sp.)
	}
	
	@Override
	public String toString() {
		return "Inputstream: "+dis.toString() + " Outputstream: "+dos.toString();
	}
}
