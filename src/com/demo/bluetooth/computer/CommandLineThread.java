package com.demo.bluetooth.computer;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class CommandLineThread implements Runnable {
	private static final int CURSOR_UP = 10;
	private static final int CURSOR_RIGHT = 20;
	private static final int CURSOR_DOWN = 30;
	private static final int CURSOR_LEFT = 40;

	public boolean hasBeenStarted;

	public CommandLineThread() {
		hasBeenStarted = false;
	}

	@Override
	public void run() {
		hasBeenStarted = true;
		while(true) {
			try {
				String line  = RemoteBluetoothServer.br.readLine();
				if(line.equals("connect")) {
					System.out.println("Instantiating Runnable");
					System.out.println("Ran Runnable(), instantiating WaitThread");
					WaitThread wt = new WaitThread();
					RemoteBluetoothServer.connections.add(wt);
					RemoteBluetoothServer.connections.get(RemoteBluetoothServer.connections.size()-1).start();
				}
				else if(line.matches("\\d u")) {
					int index = Integer.parseInt(line.substring(0, line.indexOf(' ')));
//					System.out.println(RemoteBluetoothServer.connections2.get(index).toString());
					RemoteBluetoothServer.connections2.get(index).write(CURSOR_UP);
				}
				else if(line.matches("\\d r")) {
					int index = Integer.parseInt(line.substring(0, line.indexOf(' ')));
					RemoteBluetoothServer.connections2.get(index).write(CURSOR_RIGHT);
				}
				else if(line.matches("\\d d")) {
					int index = Integer.parseInt(line.substring(0, line.indexOf(' ')));
					RemoteBluetoothServer.connections2.get(index).write(CURSOR_DOWN);
				}
				else if(line.matches("\\d l")) {
					int index = Integer.parseInt(line.substring(0, line.indexOf(' ')));
					RemoteBluetoothServer.connections2.get(index).write(CURSOR_LEFT);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
