package com.demo.bluetooth.computer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

import android.util.Log;

import com.google.zxing.WriterException;

public class WaitThread extends Thread {//implements Runnable {
	public String macAddress = "";
	private ProcessConnectionThread mProcessThread = null;
	boolean QR_enabled = true;
	public static boolean originalUUID = true;

	public Runnable mr = null;

	/** Constructor */
	public WaitThread() {
	}

	public WaitThread(boolean QR_enabled) {
		this.QR_enabled = QR_enabled;
	}

	public WaitThread(Runnable mr) {
		this.mr = mr;
	}

	@Override
	public void run() {
		if(mr != null)
			mr.run();
		waitForConnection();		
	}

	/** Waiting for connection from device(s) */
	private void waitForConnection() {
		// retrieve the local Bluetooth device object
		LocalDevice local_device = null;

		StreamConnectionNotifier notifier;
		StreamConnection connection = null;
		// setup the server to listen for connection
		try {
			local_device = LocalDevice.getLocalDevice();

			macAddress = local_device.getBluetoothAddress()
					.replaceAll(".(.)", ":$0")
					.substring(1); //gets rid of leading colon
			System.out.println("MAC: "+macAddress);

			local_device.setDiscoverable(DiscoveryAgent.GIAC);

			UUID uuid = new UUID("04c6093b00001000800000805f9b34fb", false);
			//			if(!originalUUID)
			//				uuid = new UUID("04c6093b00001000800000805f9b34fc", false);
			//			originalUUID = false;

			System.out.println("UUID: "+uuid.toString());

			String url = "btspp://localhost:" + uuid.toString() + ";name=RemoteBluetooth";
			notifier = (StreamConnectionNotifier)Connector.open(url);
		} catch (BluetoothStateException e) {
			System.out.println("Bluetooth is not turned on.");
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		// waiting for connection
		while(true) {
			if(QR_enabled) {
				//Prevents main thread from getting held up by the QR window
				Thread showQRThread = new Thread(new Runnable(){
					public void run(){
						try {
							File qrFile = new File("mac.png");
							QRGenerator.createQRImage(qrFile, macAddress, 450, "png");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
				showQRThread.start();
			}
			try {
				System.out.println("Waiting for connection...");
				connection = notifier.acceptAndOpen();

				mProcessThread = new ProcessConnectionThread(connection);
				mProcessThread.start();
				if(RemoteBluetoothServer.connections2 == null) {
					RemoteBluetoothServer.connections2 = new ArrayList<ProcessConnectionThread>();
				}
				RemoteBluetoothServer.connections2.add(mProcessThread);
				System.out.println("Just started mProcessThread");

			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}

	public void write(String message) {
		if(mProcessThread != null) {
			mProcessThread.write(message);
		}
	}

	public void write(int command) {
		if(mProcessThread != null) {
			mProcessThread.write(command);
		}
	}

	@Override
	public String toString() {
		return "This WaitThread is connected to device: "+macAddress+
				"\n with ProcessConnectionThread "+mProcessThread.toString();
	}
}
